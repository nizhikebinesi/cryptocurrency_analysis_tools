# Create a python script:
# Config :
# -Start date: start of the data capture
# -End date: end of the databcapture
# -Universe: list of coins for which we capture the data
# -Granularity: frequency of the data (30min/1hour...etc)
# -End point: item we want to capture (galaxy Score widget,
# word cloud widget...etc)
# Output :
# 1 csv file per end point (universe per columns and time per rows)
import os
from collections import defaultdict
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple, Union

import pandas as pd
import requests
from dotenv import load_dotenv

from config import PROJECT_PATH

load_dotenv(os.path.join(PROJECT_PATH, ".env"))


MODE = "w"
API_KEY: Optional[str] = os.environ.get("API_KEY", None)
END_DATE = int(datetime(2021, 6, 3).timestamp())
START_DATE = int(datetime(2021, 6, 1).timestamp())
UNIVERSE = ["BTC", "ETH"]
GRANULARITY = ["1d", "1w"]
INTERVAL = "hour"
ENDPOINT = "assets"
ENDPOINT_URL = "https://api.lunarcrush.com/v2"
# for example, ['high', 'low', 'close', 'open']
COLUMNS_OF_INTEREST: List[str] = []
DATA_STORING_FOLDER_PATH = os.path.join(PROJECT_PATH, "data")


def write_to_csv(path: str, data: pd.DataFrame, mode: str = "w") -> None:
    data.to_csv(path, mode=mode)


def get_data(
    key: str,
    data: str,
    interval: str,
    start_date: int,
    end_date: int,
    universe: List[str],
    granularity: List[str],
    endpoint: str,
) -> Optional[Dict[Any, Any]]:
    params: Dict[str, Union[str, int]] = {
        "key": key,
        "data": data,
        "end": end_date,
        "start": start_date,
        "symbol": ",".join(universe),
        "interval": interval,  # hour or day
        "change": ",".join(granularity),
    }
    result = None
    try:
        r = requests.get(endpoint, params=params)
        r.raise_for_status()
        result = r.json()
    except Exception as e:
        print(e)
    return result


def get_time_series(
    data: Dict[Any, Any], columns_of_interest: List[str],
) -> Tuple[Dict[Any, Any], List[Any]]:
    infos = data["data"]
    if len(columns_of_interest) == 0:
        columns_of_interest = list(
            set(infos[0]["timeSeries"][0].keys()) - {"time", "asset_id"}
        )

    d: Dict[Any, Any] = defaultdict(lambda: defaultdict(list))
    times: List[Any] = []

    for info in infos:
        series = info["timeSeries"]
        for point in series:
            if len(times) < len(series):
                times.append(point["time"])
            for c in columns_of_interest:
                d[c][info["symbol"]].append(point[c])
    return d, times


def create_dataframe(data: Dict[Any, Any], times: List[Any]) -> pd.DataFrame:
    df = pd.DataFrame.from_dict(data)
    df.index = times
    return df


def saving_dataframe_pipeline(
    data: Dict[Any, Any],
    times: List[Any],
    interest_name: str,
    folder_path: str,
    mode: str = "w",
) -> None:
    path = f"interest={interest_name}_from={times[0]}_to={times[-1]}.csv"
    path = os.path.join(folder_path, path)
    df = create_dataframe(data, times)
    write_to_csv(path, df, mode)


def saving_interests_to_csv(
    data: Dict[Any, Any], times: List[Any], folder_path: str, mode: str = "w"
) -> None:
    for interest in data:
        datum = data[interest]
        saving_dataframe_pipeline(datum, times, interest, folder_path, mode)


if __name__ == "__main__":
    if isinstance(API_KEY, str):
        data = get_data(
            API_KEY,
            ENDPOINT,
            INTERVAL,
            START_DATE,
            END_DATE,
            UNIVERSE,
            GRANULARITY,
            ENDPOINT_URL,
        )
        if data:
            data_for_series, times = get_time_series(data, [])
            if not os.path.exists(DATA_STORING_FOLDER_PATH):
                os.mkdir(DATA_STORING_FOLDER_PATH)
            saving_interests_to_csv(
                data_for_series, times, DATA_STORING_FOLDER_PATH, MODE
            )
